# Epic Online Services support for Rust

This could be the future home of an Epic Online Services for Rust crate.

(Not associated with Epic Games.)

<img src="assets/eos--rust--button-up-community--mid-game-play--earlier.png" height="500px"> <img src="assets/eos--rust--button-up-community--about-dialog--latest.png" height="500px">  
*Screenshots from "Button Up Community" (currently in development)*
*the interactive video game software provided for entertainment*
*purposes (and definitely not a mere "tech demo") built with this project.*

Learn more about Epic Online Services: https://dev.epicgames.com/docs/services/en-US/

(See below for [current project status](#current-project-status-aka-wheres-the-code).)


### Screenshots

![](assets/eos--rust--button-up-community--audio--wine-linux.png) *Description: Two "Button Up Community" players, one running under WINE (left) and one on Linux/Elementary OS (right). Not audible: player on left talking to player on right via Epic Online Services Voice.* 

The "Button Up Community" GUI is built with [`egui`](https://crates.io/crates/egui).

### Current project status (a.k.a "Where's the code?")

While it has been my intention from the start of development on this
project (and the sibling [Epic Online Services support for Godot addon](https://gitlab.com/RancidBacon/epic-online-services-for-godot/))
to release it under an Open Source license, the "Epic Online
Services support for Rust" project does not yet have a source
release/download available.

This is for two primary reasons:

 * Economic

   Current economic realities mean I am not in a position to support
   development of either EOS related project with solely my own
   resources.

   To get the "EOS for Godot" project to the point where it supports
   even the feature sub-set used in "The Endov Society" has taken a
   significant investment of time & effort.
   
   For this "EOS for Rust" project while I was able to build on the
   experience gained while developing "The Endov Society" and the
   Godot add-on, it has still required significant additional effort
   to support the feature sub-set used by "Button Up Community".

   Significant effort is still needed to support the rest of the
   EOS feature set; provide an idiomatic Rust interface; create
   Rust/egui/Bevy-specific examples of integrating EOS features
   and document their use.

   (See [here](https://gitlab.com/RancidBacon/epic-online-services-for-godot/-/blob/main/README.md#current-project-status-aka-wheres-the-code)
   for more background on the development process so far.)

   My goal is to release the "Button Up Community" game (as I did
   with "The Endov Society" for the Godot addon--although that has
   a more elaborate scope) as an entertaining and interactive
   way to... show... what is possible with the project and gauge the
   level of financial support to fund further development & release.

   Not really a fan of this approach but I still gotta eat and keep a
   roof over my head.

   So, here we are. :)

 * Legal

   While less complex than the issues associated with the FFI-based
   approach of the EOS for Godot addon, there are still uncertainties
   around how the EOS SDK License agreement interacts with crate
   naming and the crate build process.
   
   So it is unclear whether Epic Games would permit distribution
   of part or all of "Epic Online Services for Rust" crate(s) under
   an Open Source license--nor how litigious they might be about it. ;)

If you or your company would like to financially support the
development & release of "Epic Online Services for Rust" crate(s)
under an Open Source license please get in touch via Twitter
(https://twitter.com/rancidbacon) or create an issue in this
repository (<https://gitlab.com/RancidBacon/epic-online-services-for-rust/-/issues>).
   
----

https://gitlab.com/RancidBacon/epic-online-services-for-rust
